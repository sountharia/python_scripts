# for i in range(1,250):
#     #print('i=',i)
#     num = i
#     flag = 'x'
#     if i == 1:
#         print(num, end=',')
#         continue;
#     for j in range(2,i):
#         #print('j=',j)
#         if j != i:
#             #print('i,j=',i,j)
#             if i%j == 0:
#                 #print("divisible : ",i,j)
#                 flag = 'y'
#                 break
#     if flag == 'x':
#         #print("prime " ,num)#, end=',')
#         print(num, end=',')



# #open a file named results.txt
# try:
#     f=open("results.txt",'w')
#     f.write("Prime Numbers from 1 to 250:\n")
#     f.close
#     f=open("results.txt",'a')
# except:
#     print("Cannot open file, Check for Permission")
# else:

#     # Check each number between 1 and 250
#     for i in range(1,250):
#         num = i
#         flag = 'prime' # set flag as prime initially
#         if i == 1: #special case for number 1
#             f.write(str(num)+'\n')
#             print(num, end=',')
#             continue;
#         for j in range(2,i):
#             if j != i:
#                 if i%j == 0:
#                     flag = 'not prime'
#                     break
#         if flag == 'prime':
#             print(num, end=',')
#             f.write(str(num)+'\n')
#     f.close()
# finally:
#     print("File 'Results.txt' Created...")



#open a file named results.txt
try:
    f=open("results.txt",'w')
    f.write("Prime Numbers from 1 to 250:\n")
    f.close
    f=open("results.txt",'a')
except:
    print("Cannot open file, Check for Permission")
else:

    print("Prime Numbers from 1 to 250:")
    # Check each number between 1 and 250
    for i in range(1,250):
        num = i
        flag = 'prime' # set flag as prime initially
        if i == 1: #special case for number 1
            f.write(str(num)+'\n')
            print(num, end=' ')
            primeNumCount = 1
            continue;
        for j in range(2,i):# for every number in i check for divisiblity from 2 till that number
            if j != i: # to avoid checking same number as prime numbers can be divisible by itself
                if i%j == 0:#if the number i is divisible  then we know it is not a prime number
                    flag = 'not prime'#set the flag to not prime and break from this loop
                    break
        if flag == 'prime': #after checking the divisiblity from 2 till i, if the flag has not changed then it is a prime number
            primeNumCount += 1 #to create a new line after every 10 prime numbers created a counter
            print(num, end=' ')
            f.write(str(num)+'\n')#write the prime number in the file
        if primeNumCount%10==0 and flag=='prime':#create a new line for every 10 prime number printed
            print('\t')
    f.close()
    print("\nFile 'results.txt' Created...")

