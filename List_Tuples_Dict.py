fruits = ['apple','banana','cherry']
print(fruits)
print(type(fruits))
print(fruits[2])
fruits[1]='strawberry'

#Tuples are list that cannot be changed
animal = ('Dog','Cat','Lion')
print(animal)
print(type(animal))
print(animal[2])
print("*******************")
a=[1,2,'sou',True,[1,2,3]]
for i in a:
    print("{} is datatype {}".format(i, type(i)))
print("*******************")

#dict list with named positions
famDict = {
    "Mother" : "Sou",
    "Father" : "Arun",
    "Child"  : "Nithi"
}
print(famDict)
print(famDict["Mother"])

print(len(famDict))

for i,j in famDict.items():
    print("{} is {}".format(i, j))