import csv
import copy

myVehicle = {
    "vin"  : "<empty>",
    "make" : "<empty>",
    "model": "<empty>",
    "year" : 0,
    "range" : 0,
    "topSpeed"  : 0,
    "zeroSixty" : 0.0,
    "mileage"   : 0
}

for key, value in myVehicle.items():
    print("{}  :  {}".format(key, value))

myInventory = []    

with open ("car_fleet.csv") as csf:
    csr = csv.reader(csf)
    for i in csr:
        if csr.line_num == 1:
            print(f'{",".join(i)}')
        print(f'vin : {i[0]},  make : {i[1]}, model : {i[2]}, year : {i[3]} , range : {i[4]}, topspeed : {i[5]}, ZeroSixty : {i[6]}, mileage : {i[7]}')
        currVehicle = copy.deepcopy(myVehicle)
        currVehicle["vin"]       = i[0]
        currVehicle["make"]      = i[1]
        currVehicle["model"]     = i[2]
        currVehicle["year"]      = i[3]
        currVehicle["range"]     = i[4]
        currVehicle["topSpeed"]  = i[5]
        currVehicle["zeroSixty"] = i[6]  
        currVehicle["mileage"]   = i[7]
        myInventory.append(currVehicle)
        
    for i in myInventory:
        for j, k in i.items():
            print(f'{j} : {k}')
        print("------------------")
    