# import json
# def jsonread(filename):
#     try:
#         with open(filename) as f:
#             #"insulin.json",'r')as f:
#             a=json.load(f)
#             # TODO: write code...
#     except Exception, e:
#         raise e
    

import json

def readJsonFile(fileName):
    data = ""
    try:
        with open(fileName) as json_file:
            data = json.load(json_file)
    except IOError:
        print("Could not read file")
    return data